/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.projects.fixturelistgenerator.model.player;

/**
 *
 * @author john
 */
public interface IPlayer {

    String getPlayerName();

    void setPlayerName(String playerName);
    
}

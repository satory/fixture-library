/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.projects.fixturelistgenerator;

import org.jmcdonnell.projects.fixturelistgenerator.FixtureListGeneratorApi;
import java.util.List;
import org.jmcdonnell.projects.fixturelistgenerator.model.AdvancedFixture;
import org.jmcdonnell.projects.fixturelistgenerator.model.IAdvFixture;
import org.jmcdonnell.projects.fixturelistgenerator.model.IBasicFixture;
import org.jmcdonnell.projects.fixturelistgenerator.model.player.PlayerBuilder;
import org.jmcdonnell.projects.fixturelistgenerator.model.team.TeamBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;

/**
 *
 * @author john
 */
public class FixtureListGeneratorApiTest {
    
    private static final Integer EVEN_NUM_TEAMS = 10;
    private static final Integer EVEN_NUM_PLAYERS = 4;
    
    private FixtureListGeneratorApi unitUnderTest;
    
    @Before
    public void setUp() {
        unitUnderTest = new FixtureListGeneratorApi();
    }
    
    @Test
    public void testLibraryReturnsFixtures() {
        List<IBasicFixture> fixtures = unitUnderTest.generateFixtures(generateTestTeams());
        Assert.assertFalse(fixtures.isEmpty());
        Integer expectedSize = getExpectedSize(2, EVEN_NUM_TEAMS);
        Assert.assertTrue(fixtures.size() == expectedSize);
    }
    
    @Test
    public void testLibraryReturnsFixturesWithPlayers() {
        List<IAdvFixture> fixtures = unitUnderTest.generateFixtures(generateTestPlayers(), generateTestTeams());
        Assert.assertFalse(fixtures.isEmpty());
        Integer expectedSize = getExpectedSize(EVEN_NUM_PLAYERS, EVEN_NUM_TEAMS);
        Assert.assertTrue(fixtures.size() == expectedSize);
    }
    
    private PlayerBuilder generateTestPlayers() {
        PlayerBuilder playerBuilder 
                = new PlayerBuilder();
        
        for (int idx = 0; idx < EVEN_NUM_PLAYERS; idx++) {
            playerBuilder.addPlayer("Player" + idx);
        }
        return playerBuilder;
    }
    
    private TeamBuilder generateTestTeams() {
        TeamBuilder teamBuilder = new TeamBuilder();
        for (int idx = 0; idx < EVEN_NUM_TEAMS; idx++) {
            teamBuilder.addTeam("Team" + idx);
        }
        return teamBuilder;
    }

    private Integer getExpectedSize(Integer numOfPlayers, Integer numOfTeams) {
        Integer expectedSize = 0;
        // 2 due to the fact hat only 2 teams play in a fixture
        if (numOfTeams % numOfPlayers != 0) {
            Integer remainder = (numOfTeams % numOfPlayers);
            expectedSize = (numOfTeams - remainder) / 2;
        } else {
            expectedSize = numOfTeams / 2;
        }
        
        return expectedSize;
    }
}

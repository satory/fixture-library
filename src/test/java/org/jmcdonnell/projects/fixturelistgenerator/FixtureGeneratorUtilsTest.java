/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.projects.fixturelistgenerator;

import org.jmcdonnell.projects.fixturelistgenerator.FixtureGeneratorUtils;
import java.util.List;
import org.jmcdonnell.projects.fixturelistgenerator.model.team.ITeam;
import org.jmcdonnell.projects.fixturelistgenerator.model.team.TeamBuilder;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class FixtureGeneratorUtilsTest {
    
    @Test
    public void testUtilsCalculateNumOfFixtures() {
        assertEquals(5l, FixtureGeneratorUtils.calculateNumOfFixturesPerPlayer(10, 2).longValue());
        assertEquals(15l, FixtureGeneratorUtils.calculateNumOfFixturesPerPlayer(30, 2).longValue());
        assertEquals(3l, FixtureGeneratorUtils.calculateNumOfFixturesPerPlayer(10, 3).longValue());
        assertEquals(15l, FixtureGeneratorUtils.calculateNumOfFixturesPerPlayer(30, 2).longValue());
        assertEquals(5l, FixtureGeneratorUtils.calculateNumOfFixturesPerPlayer(10, 2).longValue());
    }
    
    @Test
    public void testUtilsCalculateOddNumberOfFixtures() {
        assertEquals(1l, FixtureGeneratorUtils.calculateOddNumberOfTeamsToRemove(10, 3).longValue());
    }
    
    @Test
    public void testUtilsRemoveListMethod() {
        TeamBuilder teamBuilder = new TeamBuilder().addTeam("team1").addTeam("team2").addTeam("team3").addTeam("team4");
        
        List<ITeam> build = teamBuilder.build();
        
        List<ITeam> removeTeams = FixtureGeneratorUtils.removeTeams(build, 2);
        
        assertTrue(removeTeams.size() == build.size() - 2);
        
    }
}

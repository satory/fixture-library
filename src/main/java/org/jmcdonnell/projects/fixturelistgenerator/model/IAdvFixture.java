/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.projects.fixturelistgenerator.model;

import org.jmcdonnell.projects.fixturelistgenerator.model.player.IPlayer;

/**
 *
 * @author john
 */
public interface IAdvFixture extends IBasicFixture {

    IPlayer getAwayPlayer();

    IPlayer getHomePlayer();

    void setAwayPlayer(IPlayer awayPlayer);

    void setHomePlayer(IPlayer homePlayer);
    
}

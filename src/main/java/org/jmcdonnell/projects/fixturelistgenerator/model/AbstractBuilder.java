/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.projects.fixturelistgenerator.model;

import java.util.List;

/**
 *
 * @author john
 * @param <T>
 */
public abstract class AbstractBuilder<T> {
    public abstract List<T> build();
}

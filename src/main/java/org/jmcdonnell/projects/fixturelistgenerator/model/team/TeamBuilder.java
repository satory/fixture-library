/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.projects.fixturelistgenerator.model.team;

import java.util.ArrayList;
import java.util.List;
import org.jmcdonnell.projects.fixturelistgenerator.model.AbstractBuilder;

/**
 *
 * @author john
 */
public class TeamBuilder extends AbstractBuilder<ITeam> {
    private final List<ITeam> teams = new ArrayList<>();
    
    public TeamBuilder addTeam(String teamName) {
        ITeam team = new Team();
        team.setTeamName(teamName);
        this.teams.add(team);
        return this;
    }

    @Override
    public List<ITeam> build() {
        return teams;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.projects.fixturelistgenerator.model.player;

import java.util.ArrayList;
import java.util.List;
import org.jmcdonnell.projects.fixturelistgenerator.model.AbstractBuilder;

/**
 *
 * @author john
 */
public class PlayerBuilder extends AbstractBuilder<IPlayer>{
    private final List<IPlayer> players = new ArrayList<>();
    
    public PlayerBuilder addPlayer(String playerName) {
        IPlayer player = new Player();
        player.setPlayerName(playerName);
        this.players.add(player);
        return this;
    }

    @Override
    public List<IPlayer> build() {
        return players;
    }
}

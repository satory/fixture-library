/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.projects.fixturelistgenerator.model;

import java.util.Objects;
import org.jmcdonnell.projects.fixturelistgenerator.model.team.ITeam;


public class BasicFixture implements IBasicFixture {
    private ITeam homeTeam;
    private ITeam awayTeam;

    @Override
    public ITeam getHomeTeam() {
        return homeTeam;
    }

    @Override
    public void setHomeTeam(ITeam homeTeam) {
        this.homeTeam = homeTeam;
    }

    @Override
    public ITeam getAwayTeam() {
        return awayTeam;
    }

    @Override
    public void setAwayTeam(ITeam awayTeam) {
        this.awayTeam = awayTeam;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.homeTeam);
        hash = 31 * hash + Objects.hashCode(this.awayTeam);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BasicFixture other = (BasicFixture) obj;
        if (!Objects.equals(this.homeTeam, other.homeTeam)) {
            return false;
        }
        if (!Objects.equals(this.awayTeam, other.awayTeam)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return homeTeam + " Vs. " + awayTeam + System.getProperty("line.separator");
    }
}

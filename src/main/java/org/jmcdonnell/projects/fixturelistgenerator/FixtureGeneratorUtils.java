/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.projects.fixturelistgenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jmcdonnell.projects.fixturelistgenerator.model.team.ITeam;

/**
 *
 * @author john
 */
class FixtureGeneratorUtils {
    
    public static Integer calculateNumOfFixturesPerPlayer(Integer numOfTeams, Integer numOfPlayers) {
        return numOfTeams / numOfPlayers;
    }
    
    public static Integer calculateOddNumberOfTeamsToRemove(Integer numOfTeams, Integer numOfPlayers) {
        return numOfTeams % numOfPlayers;
    }

    public static List<ITeam> removeTeams(List<ITeam> teams, Integer numberOfTeamsToRemove) {
        List<ITeam> modifiedTeamList = new ArrayList<>(teams.size() - numberOfTeamsToRemove);
        
        Collections.shuffle(teams);
        
        for (int idx = 0; idx < teams.size() - numberOfTeamsToRemove; idx++) {
            modifiedTeamList.add(teams.get(idx));
        }
        
        return modifiedTeamList;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.projects.fixturelistgenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jmcdonnell.projects.fixturelistgenerator.model.AdvancedFixture;
import org.jmcdonnell.projects.fixturelistgenerator.model.BasicFixture;
import org.jmcdonnell.projects.fixturelistgenerator.model.IAdvFixture;
import org.jmcdonnell.projects.fixturelistgenerator.model.IBasicFixture;
import org.jmcdonnell.projects.fixturelistgenerator.model.player.IPlayer;
import org.jmcdonnell.projects.fixturelistgenerator.model.player.PlayerBuilder;
import org.jmcdonnell.projects.fixturelistgenerator.model.team.ITeam;
import org.jmcdonnell.projects.fixturelistgenerator.model.team.TeamBuilder;

/**
 *
 * @author John McDonnell
 */

public class FixtureListGeneratorApi {
    
    private final static Integer DEFAULT_NUM_OF_PLAYERS = 2;
    
    List<IBasicFixture> generateFixtures(TeamBuilder teamBuilder) {
        List<ITeam> teams = teamBuilder.build();
        
        removeExcessTeamsIfNeeded(teams, DEFAULT_NUM_OF_PLAYERS);
        
        Collections.shuffle(teams);
        
        List<IBasicFixture> fixtures = new ArrayList<>(teams.size() / DEFAULT_NUM_OF_PLAYERS);
        
        generateFixturesAndAddTeams(teams, fixtures);
      
        return fixtures;
    }

    private void generateFixturesAndAddTeams(List<ITeam> teams, List<IBasicFixture> fixtures) {
        for (int idx = 0; idx < teams.size(); idx = idx + 2) {
            IBasicFixture fixture = new BasicFixture();
            
            fixture.setHomeTeam(teams.get(idx));
            fixture.setAwayTeam(teams.get(idx + 1));
            
            fixtures.add(fixture);
        }
    }

    private List<ITeam> removeExcessTeamsIfNeeded(List<ITeam> teams, Integer numOfPlayers) {
        if (isThereAOddNumberOfTeamsToPlayers(teams.size(), numOfPlayers)) {
            Integer numberOfTeamsToRemove = FixtureGeneratorUtils.calculateOddNumberOfTeamsToRemove(teams.size(), numOfPlayers);
            return FixtureGeneratorUtils.removeTeams(teams, numberOfTeamsToRemove);
        }
        return teams;
    }

    private boolean isThereAOddNumberOfTeamsToPlayers(Integer numOfTeams, Integer numOfPlayers) {
        return FixtureGeneratorUtils.calculateOddNumberOfTeamsToRemove(numOfTeams, numOfPlayers) != 0;
    }

    List<IAdvFixture> generateFixtures(PlayerBuilder playerBuilder, TeamBuilder teamBuilder) {
        List<ITeam> teams = teamBuilder.build();
        List<IPlayer> players = playerBuilder.build();
        
        teams = removeExcessTeamsIfNeeded(teams, players.size());
        
        Collections.shuffle(teams);
        
        List<IBasicFixture> fixtures = new ArrayList<>(teams.size() / players.size());
        
        generateFixturesAndAddTeams(teams, fixtures);
        
        List<IAdvFixture> advFixtures = convertFixturesToAdvancedFixtures(fixtures);
        
        return advFixtures;
    } 

    private List<IAdvFixture> convertFixturesToAdvancedFixtures(List<IBasicFixture> fixtures) {
        List<IAdvFixture> advFixtures = new ArrayList<>(fixtures.size());
        
        for (IBasicFixture fixture : fixtures) {
            advFixtures.add(AdvancedFixture.convertTo(fixture));
        }
        
        return advFixtures;
    }
}

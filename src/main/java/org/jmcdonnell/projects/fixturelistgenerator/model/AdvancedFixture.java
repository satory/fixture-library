/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.projects.fixturelistgenerator.model;

import java.util.Objects;
import org.jmcdonnell.projects.fixturelistgenerator.model.player.IPlayer;

/**
 *
 * @author john
 */
public class AdvancedFixture extends BasicFixture implements IAdvFixture {
    private IPlayer homePlayer;
    private IPlayer awayPlayer;

    @Override
    public IPlayer getHomePlayer() {
        return homePlayer;
    }

    @Override
    public void setHomePlayer(IPlayer homePlayer) {
        this.homePlayer = homePlayer;
    }

    @Override
    public IPlayer getAwayPlayer() {
        return awayPlayer;
    }

    @Override
    public void setAwayPlayer(IPlayer awayPlayer) {
        this.awayPlayer = awayPlayer;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.getHomePlayer());
        hash = 37 * hash + Objects.hashCode(this.getHomeTeam());
        hash = 37 * hash + Objects.hashCode(this.getAwayPlayer());
        hash = 37 * hash + Objects.hashCode(this.getAwayTeam());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AdvancedFixture other = (AdvancedFixture) obj;
        if (!Objects.equals(this.getHomePlayer(), other.getHomePlayer())) {
            return false;
        }
        if (!Objects.equals(this.getHomeTeam(), other.getHomeTeam())) {
            return false;
        }
        if (!Objects.equals(this.getAwayPlayer(), other.getAwayPlayer())) {
            return false;
        }
        if (!Objects.equals(this.getAwayTeam(), other.getAwayTeam())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getHomeTeam() + " (" + this.getHomePlayer() +") Vs. " + this.getAwayTeam() + " (" + this.getAwayPlayer() + ")" + System.getProperty("line.separator");
    }
    
    public static IAdvFixture convertTo(IBasicFixture fixture) {
        IAdvFixture advFixture = new AdvancedFixture();
        advFixture.setHomeTeam(fixture.getHomeTeam());
        advFixture.setAwayTeam(fixture.getAwayTeam());
        
        return advFixture;
    }
}

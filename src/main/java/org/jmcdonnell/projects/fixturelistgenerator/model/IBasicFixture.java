/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmcdonnell.projects.fixturelistgenerator.model;

import org.jmcdonnell.projects.fixturelistgenerator.model.team.ITeam;

/**
 *
 * @author john
 */
public interface IBasicFixture {

    ITeam getAwayTeam();

    ITeam getHomeTeam();

    void setAwayTeam(ITeam awayTeam);

    void setHomeTeam(ITeam homeTeam);
    
}
